from textwrap import wrap
from typing import Tuple


class MFC:
    def __init__(self, data: str):
        self.data = data
        self.size = self._determine_size()
        self.sectors = self._split_into_sectors()

    def _determine_size(self):
        if len(self.data) == 2048:
            return 1024
        elif len(self.data) == 8192:
            return 4096
        else:
            raise ValueError("Dump size isn't 1k/4k.")

    def _split_into_sectors(self):
        # organized in 32 sectors of 4 blocks and 8 sectors of 16 blocks
        sectors = wrap(self.data[0:2048], 128)
        # On 4k cards, get the rest of sectors too
        if self.size == 4096:
            sectors += wrap(self.data[2048:8192], 512)
        return sectors

    def get_block(self, sector_number: int, block_number: int) -> str:
        sector = self.sectors[sector_number]
        block = wrap(sector, 32)
        return block[block_number]

    def get_trailer(self, sector_number: int) -> str:
        return self.get_block(sector_number, -1)

    def get_keys(self, sector_number: int) -> Tuple[str, str]:
        trailer = self.get_trailer(sector_number)
        keya = trailer[0:12]
        keyb = trailer[-12:]
        return keya, keyb
