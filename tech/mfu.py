from textwrap import wrap


class MFU:
    def __init__(self, data: str):
        self.data = data

        self.size = self._determine_size()
        self.blocks = self._split_into_blocks()
        self.uid = self._extract_uid()

        # TODO: Does not account for C
        self.pwd = self._determine_pwd()
        # TODO: Determine tech by size and UID like pm3 client

    def _determine_size(self):
        # TODO: Account for more improper sizes
        assert len(self.data) % 4 == 0
        # 4 bytes per block * 2 bytes for hex repr
        size = int(len(self.data) / 8)
        return size

    def _determine_pwd(self):
        # EV1 small and big
        if self.size in [20, 41]:
            return self.blocks[-2]

        # TODO: Account for my-d move, potentially others too. MIKRON?

        return False

    def _split_into_blocks(self):
        blocks = wrap(self.data, 8)
        return blocks

    def _extract_uid(self):
        uid1 = self.blocks[0][0:6]
        uid2 = self.blocks[1]
        return uid1 + uid2
