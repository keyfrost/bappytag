import defusedxml.ElementTree as ET

# "/home/ave/Projects/nfc/keyfrost/dumps/mfc/7b1k/taginfoandroid/01-02-03-04-05-06-07_2020-11-15_21-15-41_taginfo_scan.xml"
# "/home/ave/Projects/nfc/keyfrost/dumps/mfc/4b1k/taginfoandroid/allkeysknown/01-02-03-04_2020-11-15_20-20-43_taginfo_scan.xml"
# "/home/ave/Projects/nfc/keyfrost/dumps/mfc/4b1k/taginfoandroid/nokeysknown/D5-68-B6-4E_2020-11-15_20-31-22_taginfo_scan.xml"
# "/home/ave/Projects/nfc/keyfrost/dumps/mfc/7b4k/taginfoandroid/01-02-03-04-05-06-07_2020-11-15_21-16-28_taginfo_scan.xml"


def parse(file_text: str):
    root = ET.fromstring(file_text)

    mem = root.find('.//subsection[@title="Memory content"]')
    memtext = ""

    for child in mem:
        if child.attrib["type"] not in ["MifareData", "MifareTrailer"]:
            continue

        cont = child.find("data").text

        if child.attrib["type"] == "MifareTrailer":
            keya = child.find("keyA").text
            keyb = child.find("keyB").text

            # Insert keya/keyb manually
            if keya:
                cont = keya + cont[len(keya):]
            if keyb:
                cont = cont[:-len(keyb)] + keyb

        if cont:
            memtext += cont.replace(" ", "")
        else:
            memtext += "?" * 32

    return memtext
