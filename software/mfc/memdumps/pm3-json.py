import json

# "/home/ave/Projects/nfc/keyfrost/dumps/mfc/4b1k/pm3/test2/hf-mf-01020304-dump.json"


def parse(file_text: str):
    file_cont = json.loads(file_text)
    data = "".join(file_cont["blocks"].values())
    return data
