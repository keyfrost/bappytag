# "../../dumps/mfc/4b1k/mct/2020-11-15_20-36-01"
# "../../dumps/mfc/7b4k/mct/2020-11-15_21-11-50"

def parse(file_text: str):
    # break into sectors
    sectors = file_text.split("+Sector: ")[1:]
    # sectors = [''.join(sector.split("\n")[1:]) for sector in sectors]
    data = ''.join([''.join(sector.split("\n")[1:]) for sector in sectors])
    return data
