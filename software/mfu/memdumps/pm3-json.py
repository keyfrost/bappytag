import json

# "/home/ave/Projects/nfc/keyfrost/dumps/mfu/mfu/pm3/hf-mfu-042DEC12364980-dump.json"


def parse(file_text: str):
    file_cont = json.loads(file_text)
    data = "".join(file_cont["blocks"].values())
    return data
