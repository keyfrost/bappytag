import defusedxml.ElementTree as ET

# "/home/ave/Projects/nfc/keyfrost/dumps/mfu/mfu/taginfoandroid/04-2D-EC-12-36-49-80_2020-11-16_00-17-50_taginfo_scan.xml"


def parse(file_text: str):
    root = ET.fromstring(file_text)

    mem = root.find('.//subsection[@title="Memory content"]')
    memtext = ""

    for child in mem:
        if child.attrib["type"] not in ["Ultralight"]:
            continue

        cont = child.find("data").text

        if cont:
            cont = cont.replace(" ", "")
            # Pad with 0s when applicable
            if len(cont) != 8:
                cont += ("0" * (8 - len(cont)))
            memtext += cont
        else:
            memtext += "?" * 8

    return memtext
