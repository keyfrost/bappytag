# bappytag

*"it do the bleepybloops" -kat*

Bappytag is a project to parse dumps of various NFC tags from a number of software.

## Supported tag tech

- Mifare Classic (1K, 4K, EV1 etc)
- Mifare Ultralight and some other NFC Forum Type 2 Tags (Ultralight, Ultralight EV1, Inf my-d move lean)

## Supported software

### Mifare Classic

- Mifare Classic Tool dumps
- Proxmark3 bin dumps
- Proxmark3 eml dumps
- Proxmark3 json dumps
- Proxmark3 -key.bin dumps (keys only)
- NXP TagInfo XML dumps

### Mifare Ultralight and other NFC Forum Type 2 Tags

- Proxmark3 bin dumps
- Proxmark3 eml dumps
- Proxmark3 json dumps
- NXP TagInfo XML dumps
- MIFARE++ Ultralight dumps

## Supported data

### Mifare Classic

- Tag size detection
- Sectors
- Blocks of sectors
- Trailers of sectors
- Keys of sectors

### Mifare Ultralight

- Tag size detection
- UID
- Blocks
- Password
